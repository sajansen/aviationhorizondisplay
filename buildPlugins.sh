#!/bin/bash

function build() {
  FILE="$1"
  NAME="$2"

  echo "Building ${NAME}..."
  echo ""
  /snap/bin/kotlinc "${FILE}" -d "./plugins/${NAME}".jar -cp '.:./target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
}

if [ -z "$1" ]; then
  for filename in ./plugins/sources/*.kt; do
    NAME=$(basename "$filename")
    build "${filename}" "${NAME%.kt}"
  done
else
  filename="$1"
  NAME=$(basename "$filename")
  build "${filename}" "${NAME%.kt}"
fi
