import nl.sajansen.graphics.createGraphics
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.Polygon
import java.awt.geom.Arc2D
import java.awt.image.BufferedImage
import kotlin.math.absoluteValue

/**
 * Usage for compiling:
 * /snap/bin/kotlinc DefaultTurnIndicator.kt -d ../DefaultTurnIndicator.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class DefaultTurnIndicator : ScreenElementPlugin {
    override val pluginName: String
        get() = "DefaultTurnIndicator"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType
        get() = ScreenElementType.TURN_INDICATOR

    override var left: Int = 0
    override var top: Int = 0
    override var width: Int = 460
    override var height: Int = 0

    private val curveDistanceToTop = 50
    private val bankArrowWidth = 20
    private val bankPredictionArrowMargin = 3
    private val bankPredictionArrowWidth = 5
    private val largeDegrees = listOf(30, 60)
    private val smallDegrees = listOf(10, 20, 45)
    private val maxBankDegree = 60
    private val turnCoordinatorMaxDeviationDegree = 15.0

    override var previewImageSource: String? = "./DefaultTurnIndicator.png"
    override var previewBufferedImage: BufferedImage? = null

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        height = screenProperties.height
        left = (screenProperties.width / 2.0 - width / 2.0).toInt()

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(1F)

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)

        // Draw the static part
        val currentBankArrow = Polygon()
        currentBankArrow.addPoint(centerPoint.x.toInt(), curveDistanceToTop)
        currentBankArrow.addPoint((centerPoint.x + bankArrowWidth / 2.0).toInt(), curveDistanceToTop + bankArrowWidth)
        currentBankArrow.addPoint((centerPoint.x - bankArrowWidth / 2.0).toInt(), curveDistanceToTop + bankArrowWidth)

        g2.color = Color.WHITE
        g2.fill(currentBankArrow)

        // Draw the dynamic/rotating part
        var originalTransformation = g2.transform
        g2.rotate(
            Math.toRadians(turnCoordinatorMaxDeviationDegree * flightData.turnCoordinator),
            bufferedImage.width / 2.0,
            bufferedImage.height / 2.0
        )

        val turnCoordinatorArrow = Polygon()
        val bankPredictionArrowTopPosition = curveDistanceToTop + bankArrowWidth + bankPredictionArrowMargin
        turnCoordinatorArrow.addPoint(
            (centerPoint.x - (bankArrowWidth + bankPredictionArrowMargin) / 2.0).toInt(),
            bankPredictionArrowTopPosition
        )
        turnCoordinatorArrow.addPoint(
            (centerPoint.x + (bankArrowWidth + bankPredictionArrowMargin) / 2.0).toInt(),
            bankPredictionArrowTopPosition
        )
        turnCoordinatorArrow.addPoint(
            (centerPoint.x + (bankArrowWidth + bankPredictionArrowMargin + bankPredictionArrowWidth) / 2.0).toInt(),
            bankPredictionArrowTopPosition + bankPredictionArrowWidth
        )
        turnCoordinatorArrow.addPoint(
            (centerPoint.x - (bankArrowWidth + bankPredictionArrowMargin + bankPredictionArrowWidth) / 2.0).toInt(),
            bankPredictionArrowTopPosition + bankPredictionArrowWidth
        )

        g2.color = Color.WHITE
        g2.fill(turnCoordinatorArrow)
        g2.transform = originalTransformation

        g2.rotate(flightData.bank.getRadians(), bufferedImage.width / 2.0, bufferedImage.height / 2.0)
        originalTransformation = g2.transform

        val curveSize = bufferedImage.height - 2.0 * curveDistanceToTop
        val curve = Arc2D.Double(
            (bufferedImage.width - curveSize) / 2.0, curveDistanceToTop.toDouble(),
            curveSize, curveSize,
            90.0 + maxBankDegree, -2.0 * maxBankDegree, Arc2D.OPEN
        )
        g2.draw(curve)

        for (bankDegree in -180..180) {
            var lineLength = 0
            if (largeDegrees.contains(bankDegree.absoluteValue)) {
                lineLength = 20
            } else if (smallDegrees.contains(bankDegree.absoluteValue)) {
                lineLength = 10
            } else if (bankDegree != 0) {
                continue
            }

            g2.transform = originalTransformation
            g2.rotate(
                Math.toRadians(bankDegree.toDouble()),
                bufferedImage.width / 2.0,
                bufferedImage.height / 2.0
            )

            if (bankDegree != 0) {
                g2.drawLine(
                    centerPoint.x.toInt(), curveDistanceToTop,
                    centerPoint.x.toInt(), curveDistanceToTop - lineLength
                )
                continue
            }

            val centerBankArrow = Polygon()
            centerBankArrow.addPoint(centerPoint.x.toInt(), curveDistanceToTop)
            centerBankArrow.addPoint(
                (centerPoint.x + bankArrowWidth / 2.0).toInt(),
                curveDistanceToTop - bankArrowWidth
            )
            centerBankArrow.addPoint(
                (centerPoint.x - bankArrowWidth / 2.0).toInt(),
                curveDistanceToTop - bankArrowWidth
            )

            g2.color = Color.WHITE
            g2.fill(centerBankArrow)
        }

        g2.dispose()
        return bufferedImage
    }

}