import nl.sajansen.graphics.createGraphics
import nl.sajansen.graphics.getNumericFontHeight
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.*
import java.awt.image.BufferedImage

/**
 * Usage for compiling:
 * /snap/bin/kotlinc DefaultAttitudeLines.kt -d ../DefaultAttitudeLines.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class DefaultAttitudeLines : ScreenElementPlugin {
    override val pluginName: String
        get() = "DefaultAttitudeLines"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType
        get() = ScreenElementType.TURN_INDICATOR

    override var left: Int = 0
    override var top: Int = 0
    override var width: Int = 370
    override var height: Int = 350

    override var previewImageSource: String? = "./DefaultAttitudeLines.png"
    override var previewBufferedImage: BufferedImage? = null

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        left = (screenProperties.width / 2.0 - width / 2.0).toInt()
        top = (screenProperties.height / 2.0 - height / 2.0).toInt()

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(0.5F)
        g2.font = Font("Dialog", Font.BOLD, 12)
        g2.rotate(flightData.bank.getRadians(), bufferedImage.width / 2.0, bufferedImage.height / 2.0)

        val pitchLineMaxWidth = 130
        val pitchLineTextMargin = 5
        val maxPitchDifference = 30
        val pitchLineHeight = 2

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)
        val horizonLevel = centerPoint.y + flightData.pitch.getDegrees() * screenProperties.pitchToScreenFactor

        val fontMetrics: FontMetrics = g2.fontMetrics

        val minPitchLinesDegree = flightData.pitch.getDegrees().toInt() - maxPitchDifference
        val maxPitchLinesDegree = flightData.pitch.getDegrees().toInt() + maxPitchDifference
        for (pitchLineDegrees in minPitchLinesDegree until maxPitchLinesDegree) {
            var pitchLineWidth: Int
            var displayPitchText = false
            if (pitchLineDegrees == 0) {
                continue
            } else if (pitchLineDegrees % 10 == 0) {
                pitchLineWidth = pitchLineMaxWidth
                displayPitchText = true
            } else if (pitchLineDegrees % 5 == 0) {
                pitchLineWidth = pitchLineMaxWidth / 2
            } else {
                continue
            }

            val pitchLinePoint =
                Point(centerPoint.x, horizonLevel - pitchLineDegrees * screenProperties.pitchToScreenFactor)
            val pitchLineRectangle = Rectangle(
                (pitchLinePoint.x - pitchLineWidth / 2).toInt(),
                (pitchLinePoint.y - pitchLineHeight / 2).toInt(),
                pitchLineWidth,
                pitchLineHeight
            )

            g2.color = Color.WHITE
            g2.fill(pitchLineRectangle)

            if (!displayPitchText) {
                continue
            }

            val pitchTextWidth = fontMetrics.stringWidth(pitchLineDegrees.toString())
            val pitchTextLeftPoint = Point(
                pitchLineRectangle.x - pitchLineTextMargin - pitchTextWidth,
                pitchLinePoint.y + getNumericFontHeight(fontMetrics) / 2
            )
            val pitchTextRightPoint = Point(
                pitchLineRectangle.x + pitchLineRectangle.width + pitchLineTextMargin,
                pitchTextLeftPoint.y
            )

            g2.drawString(pitchLineDegrees.toString(), pitchTextLeftPoint.x.toInt(), pitchTextLeftPoint.y.toInt())
            g2.drawString(pitchLineDegrees.toString(), pitchTextRightPoint.x.toInt(), pitchTextRightPoint.y.toInt())
        }

        g2.dispose()
        return bufferedImage
    }

}