import nl.sajansen.graphics.createGraphics
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import java.awt.*
import java.awt.image.BufferedImage

/**
 * Usage for compiling:
 * /snap/bin/kotlinc NotAValidInterface.kt -d ../NotAValidInterface.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class NotAValidInterface {
    val pluginName: String
        get() = "NotAValidInterface"
    val pluginDescription: String
        get() = "An invalid interface to check the ability of the PluginLoader"
    val pluginVersion: String
        get() = "1.0.0"
    val pluginAuthor: String
        get() = "Samuel-Anton Jansen"

    var left: Int = 0
    var top: Int = 0
    var width: Int = 360
    var height: Int = 45

    fun setup() {
        println("Plugin $pluginName has been enabled!")
    }

    @Suppress("UNUSED_PARAMETER")
    fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        left = (screenProperties.width / 2.0 - width / 2.0).toInt()
        top = (screenProperties.height / 2.0 - height / 2.0).toInt()

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(2F)

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)

        val arrowWidth = 150
        val arrowHeight = 20
        val arrowMiddleHeight = 12
        val arrowColor = Color(168, 0, 203)

        val attitudeCenterPolygon = Polygon()
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt(), centerPoint.y.toInt())
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt() + arrowWidth / 2, centerPoint.y.toInt() + arrowHeight)
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt(), centerPoint.y.toInt() + arrowMiddleHeight)
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt() - arrowWidth / 2, centerPoint.y.toInt() + arrowHeight)

        g2.color = arrowColor
        g2.fill(attitudeCenterPolygon)
        g2.color = Color.BLACK
        g2.draw(attitudeCenterPolygon)

        val rectangleCenterDistance = 150
        val rectangleWidth = 50
        val rectangleHeight = 6

        val leftRectangle = Rectangle(
            centerPoint.x.toInt() - rectangleWidth / 2 - rectangleCenterDistance,
            centerPoint.y.toInt() - rectangleHeight / 2, rectangleWidth, rectangleHeight
        )
        g2.color = arrowColor
        g2.fill(leftRectangle)
        g2.color = Color.BLACK
        g2.draw(leftRectangle)

        val rightRectangle = Rectangle(
            centerPoint.x.toInt() - rectangleWidth / 2 + rectangleCenterDistance,
            centerPoint.y.toInt() - rectangleHeight / 2, rectangleWidth, rectangleHeight
        )
        g2.color = arrowColor
        g2.fill(rightRectangle)
        g2.color = Color.BLACK
        g2.draw(rightRectangle)

        g2.dispose()
return bufferedImage
    }
}