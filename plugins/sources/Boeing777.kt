package nl.sajansen

import nl.sajansen.plugins.AircraftPlugin
import java.awt.image.BufferedImage

/**
 * Usage for compiling:
 * /snap/bin/kotlinc Boeing777.kt -d ../Boeing777.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class Boeing777 : AircraftPlugin {
    override val pluginName: String
        get() = "Boeing777"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"

    override val aircraftName: String
        get() = "Boeing 777"
    override val minAirspeed: Double
        get() = 137.0
    override val minAirspeedNoFlaps: Double
        get() = 200.0
    override val maxAirspeedWarningSpeed: Double
        get() = 480.0
    override val maxAirspeed: Double
        get() = 512.0

    override var previewImageSource: String? = "./Boeing777.jpg"
    override var previewBufferedImage: BufferedImage? = null
}