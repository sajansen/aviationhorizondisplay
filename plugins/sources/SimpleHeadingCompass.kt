import nl.sajansen.graphics.createGraphics
import nl.sajansen.graphics.drawImageInXCenter
import nl.sajansen.graphics.getNumericFontHeight
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.*
import java.awt.geom.Arc2D
import java.awt.image.BufferedImage
import kotlin.math.roundToInt

/**
 * Usage for compiling:
 * /snap/bin/kotlinc SimpleHeadingCompass.kt -d ../SimpleHeadingCompass.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class SimpleHeadingCompass : ScreenElementPlugin {
    override val pluginName: String = "SimpleHeadingCompass"
    override val pluginDescription: String = ""
    override val pluginVersion: String = "1.0.0"
    override val pluginAuthor: String = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType = ScreenElementType.HEADING_COMPASS

    override var left: Int = 0
    override var top: Int = 350
    override var width: Int = 230
    override var height: Int = 250

    override var previewImageSource: String? = "./SimpleHeadingCompass.jpg"
    override var previewBufferedImage: BufferedImage? = null

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        left = (screenProperties.width / 2.0 - width / 2.0).toInt()

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)

        drawImageInXCenter(g2, bufferedImage.width, drawHeadingWheel(flightData), 25)
        drawImageInXCenter(g2, bufferedImage.width, drawHeadingPredictionBar(flightData), 30)
        drawImageInXCenter(g2, bufferedImage.width, drawCurrentHeading(flightData), 0)
        drawImageInXCenter(g2, bufferedImage.width, drawAirplane(flightData), 35 + 220 / 2 - 40 / 2)

        g2.dispose()
        return bufferedImage
    }


    private fun drawCurrentHeading(flightData: FlightData): BufferedImage {
        val backgroundHeight = 30
        val arrowWidth = 12

        val (bufferedImage, g2: Graphics2D) = createGraphics(70, backgroundHeight + arrowWidth)
        g2.stroke = BasicStroke(1F)
        g2.font = Font("Dialog", Font.PLAIN, 20)
        val fontMetrics: FontMetrics = g2.fontMetrics

        val background = Rectangle(0, 0, bufferedImage.width - 1, backgroundHeight - 1)
        g2.color = Color.BLACK
        g2.fill(background)
        g2.color = Color.WHITE
        g2.draw(background)

        val headingString = String.format("%03d", flightData.heading.getDegrees().roundToInt())
        val headingStringLeftPoint = bufferedImage.width / 2 - fontMetrics.stringWidth(headingString) / 2
        val headingStringTopPoint =
            (backgroundHeight - (backgroundHeight - getNumericFontHeight(fontMetrics)) / 2).toInt()

        g2.color = Color.WHITE
        g2.drawString("$headingString°", headingStringLeftPoint, headingStringTopPoint)


        val arrow = Polygon()
        arrow.addPoint(bufferedImage.width / 2 - arrowWidth / 2, backgroundHeight)
        arrow.addPoint(bufferedImage.width / 2 + arrowWidth / 2, backgroundHeight)
        arrow.addPoint(bufferedImage.width / 2, backgroundHeight + arrowWidth)

        g2.color = Color.WHITE
        g2.fill(arrow)

        g2.dispose()
        return bufferedImage
    }

    private fun drawHeadingPredictionBar(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(220, 220)
        g2.stroke = BasicStroke(6F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER)
        g2.color = Color.PINK

        val curveSize = 212.0
        val curve = Arc2D.Double(
            (bufferedImage.width - curveSize) / 2, 4.0,
            curveSize, curveSize,
            90.0, -1 * flightData.headingIncreasePrediction.getDegrees(), Arc2D.OPEN
        )
        g2.draw(curve)

        g2.dispose()
        return bufferedImage
    }

    private fun drawHeadingWheel(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(230, 230)
        g2.font = Font("Dialog", Font.BOLD, 14)
        val fontMetrics: FontMetrics = g2.fontMetrics
        var originalTransformation = g2.transform

        val lineOuterMarge = 14
        val smallLineSize = 5
        val largeLineSize = 12
        val relativeHeadingIndicatorDegrees = listOf(10, 20, 45, 90)

        // Draw the static heading indicators
        g2.color = Color.WHITE
        g2.stroke = BasicStroke(1.5F)
        for (heading in relativeHeadingIndicatorDegrees) {
            g2.transform = originalTransformation
            g2.rotate(Math.toRadians(heading.toDouble()), bufferedImage.width / 2.0, bufferedImage.height / 2.0)

            g2.drawLine(bufferedImage.width / 2, 0, bufferedImage.width / 2, lineOuterMarge - 4)

            g2.transform = originalTransformation
            g2.rotate(Math.toRadians(-1 * heading.toDouble()), bufferedImage.width / 2.0, bufferedImage.height / 2.0)

            g2.drawLine(bufferedImage.width / 2, 0, bufferedImage.width / 2, lineOuterMarge - 4)
        }

        // Draw the full 360 degrees
        g2.transform = originalTransformation
        g2.rotate(
            Math.toRadians(360.0 - flightData.heading.getDegrees()),
            bufferedImage.width / 2.0,
            bufferedImage.height / 2.0
        )
        originalTransformation = g2.transform

        g2.color = Color.WHITE
        g2.stroke = BasicStroke(1F)
        for (heading in 0..359) {

            if (heading % 5 != 0) {
                continue
            }

            g2.transform = originalTransformation
            g2.rotate(Math.toRadians(heading.toDouble()), bufferedImage.width / 2.0, bufferedImage.height / 2.0)

            if (heading % 10 == 0) {
                g2.drawLine(
                    bufferedImage.width / 2,
                    lineOuterMarge,
                    bufferedImage.width / 2,
                    lineOuterMarge + largeLineSize
                )
            } else {
                g2.drawLine(
                    bufferedImage.width / 2,
                    lineOuterMarge,
                    bufferedImage.width / 2,
                    lineOuterMarge + smallLineSize
                )
            }

            if (heading % 30 == 0) {
                var headingString: String
                when (heading) {
                    0 -> headingString = "N"
                    90 -> headingString = "E"
                    180 -> headingString = "S"
                    270 -> headingString = "W"
                    else -> headingString = (heading / 10).toString()
                }

                val headingStringLeftPoint = bufferedImage.width / 2 - fontMetrics.stringWidth(headingString) / 2
                g2.drawString(
                    headingString,
                    headingStringLeftPoint,
                    lineOuterMarge + fontMetrics.height + largeLineSize
                )
            }
        }

        g2.dispose()
        return bufferedImage
    }

    private fun drawAirplane(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(40, 40)
        g2.stroke = BasicStroke(1F)

        val airplane = Polygon()
        airplane.addPoint(20, 0)

        airplane.addPoint(22, 3)
        airplane.addPoint(23, 4)
        airplane.addPoint(23, 11)
        airplane.addPoint(38, 16)
        airplane.addPoint(38, 19)
        airplane.addPoint(23, 19)
        airplane.addPoint(22, 28)
        airplane.addPoint(29, 31)
        airplane.addPoint(29, 33)

        airplane.addPoint(20, 33)

        airplane.addPoint(11, 33)
        airplane.addPoint(11, 31)
        airplane.addPoint(18, 28)
        airplane.addPoint(17, 19)
        airplane.addPoint(2, 19)
        airplane.addPoint(2, 16)
        airplane.addPoint(17, 11)
        airplane.addPoint(17, 4)
        airplane.addPoint(18, 3)

        g2.color = Color.WHITE
        g2.fill(airplane)

        g2.dispose()
        return bufferedImage
    }
}