import nl.sajansen.graphics.createGraphics
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.*
import java.awt.image.BufferedImage

/**
 * Usage for compiling:
 * /snap/bin/kotlinc StaticAircraftOverlayPurpleArrow.kt -d ../StaticAircraftOverlayPurpleArrow.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class StaticAircraftOverlayPurpleArrow : ScreenElementPlugin {
    override val pluginName: String
        get() = "StaticAircraftOverlayPurpleArrow"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType
        get() = ScreenElementType.STATIC_AIRCRAFT_OVERLAY

    override var left: Int = 0
    override var top: Int = 0
    override var width: Int = 360
    override var height: Int = 45

    override var previewImageSource: String? = "./StaticAircraftOverlayPurpleArrow.jpg"
    override var previewBufferedImage: BufferedImage? = null

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        left = (screenProperties.width / 2.0 - width / 2.0).toInt()
        top = (screenProperties.height / 2.0 - height / 2.0).toInt()

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(2F)

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)

        val arrowWidth = 150
        val arrowHeight = 20
        val arrowMiddleHeight = 12
        val arrowColor = Color(168, 0, 203)

        val attitudeCenterPolygon = Polygon()
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt(), centerPoint.y.toInt())
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt() + arrowWidth / 2, centerPoint.y.toInt() + arrowHeight)
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt(), centerPoint.y.toInt() + arrowMiddleHeight)
        attitudeCenterPolygon.addPoint(centerPoint.x.toInt() - arrowWidth / 2, centerPoint.y.toInt() + arrowHeight)

        g2.color = arrowColor
        g2.fill(attitudeCenterPolygon)
        g2.color = Color.BLACK
        g2.draw(attitudeCenterPolygon)

        val rectangleCenterDistance = 150
        val rectangleWidth = 50
        val rectangleHeight = 6

        val leftRectangle = Rectangle(
            centerPoint.x.toInt() - rectangleWidth / 2 - rectangleCenterDistance,
            centerPoint.y.toInt() - rectangleHeight / 2, rectangleWidth, rectangleHeight
        )
        g2.color = arrowColor
        g2.fill(leftRectangle)
        g2.color = Color.BLACK
        g2.draw(leftRectangle)

        val rightRectangle = Rectangle(
            centerPoint.x.toInt() - rectangleWidth / 2 + rectangleCenterDistance,
            centerPoint.y.toInt() - rectangleHeight / 2, rectangleWidth, rectangleHeight
        )
        g2.color = arrowColor
        g2.fill(rightRectangle)
        g2.color = Color.BLACK
        g2.draw(rightRectangle)

        g2.dispose()
        return bufferedImage
    }
}