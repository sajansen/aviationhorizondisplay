package nl.sajansen

import nl.sajansen.plugins.AircraftPlugin
import java.awt.image.BufferedImage

/**
 * Usage for compiling:
 * /snap/bin/kotlinc Cessna172.kt -d ../Cessna172.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class Cessna172 : AircraftPlugin {
    override val pluginName: String
        get() = "Cessna172"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"

    override val aircraftName: String
        get() = "Cessna 172"
    override val minAirspeed: Double
        get() = 47.0
    override val minAirspeedNoFlaps: Double
        get() = 80.0
    override val maxAirspeedWarningSpeed: Double
        get() = 170.0
    override val maxAirspeed: Double
        get() = 163.0

    override var previewImageSource: String? = "./Cessna172.jpg"
    override var previewBufferedImage: BufferedImage? = null
}