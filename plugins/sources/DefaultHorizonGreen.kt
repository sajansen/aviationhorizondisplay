import nl.sajansen.graphics.createGraphics
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.Polygon
import java.awt.image.BufferedImage
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Usage for compiling:
 * /snap/bin/kotlinc DefaultHorizonGreen.kt -d ../DefaultHorizonGreen.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class DefaultHorizonGreen : ScreenElementPlugin {
    override val pluginName: String
        get() = "DefaultHorizonGreen"
    override val pluginDescription: String
        get() = "Flat line horizon with green land and blue skies"
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType
        get() = ScreenElementType.HORIZON

    override var left: Int = 0
    override var top: Int = 0
    override var width: Int = 0
    override var height: Int = 0

    override var previewImageSource: String? = "./DefaultHorizonGreen.png"
    override var previewBufferedImage: BufferedImage? = null

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        setDimensions(screenProperties)

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(2F)
        g2.rotate(flightData.bank.getRadians(), bufferedImage.width / 2.0, bufferedImage.height / 2.0)

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)
        centerPoint.y += flightData.pitch.getDegrees() * screenProperties.pitchToScreenFactor

        val leftCenterPoint = Point(centerPoint.x - bufferedImage.width / 2, centerPoint.y)
        val rightCenterPoint = Point(centerPoint.x + bufferedImage.width / 2, centerPoint.y)

        val groundPolygon = Polygon()
        groundPolygon.addPoint(0, bufferedImage.height)
        groundPolygon.addPoint(leftCenterPoint.x.toInt(), leftCenterPoint.y.toInt())
        groundPolygon.addPoint(rightCenterPoint.x.toInt(), rightCenterPoint.y.toInt())
        groundPolygon.addPoint(bufferedImage.width, bufferedImage.height)

        // Paint blue sky
        g2.color = Color.BLUE
        g2.fillRect(0, 0, bufferedImage.width, bufferedImage.height)
        // Paint ground overlay
        g2.color = Color(2, 112, 0)
        g2.fillPolygon(groundPolygon)
        // Paint horizon
        g2.color = Color.WHITE
        g2.drawLine(
            leftCenterPoint.x.toInt(), leftCenterPoint.y.toInt(),
            rightCenterPoint.x.toInt(), rightCenterPoint.y.toInt()
        )

        g2.dispose()
        return bufferedImage
    }

    private fun setDimensions(screenProperties: ScreenProperties) {
        if (width == 0 || height == 0) {
            width = sqrt(
                screenProperties.width.toDouble().pow(2.0)
                        + screenProperties.height.toDouble().pow(2.0)
            ).toInt()
            height = width
        }

        left = (screenProperties.width / 2.0 - width / 2.0).toInt()
        top = (screenProperties.height / 2.0 - height / 2.0).toInt()
    }

}