import nl.sajansen.graphics.createGraphics
import nl.sajansen.graphics.drawImageInYCenter
import nl.sajansen.graphics.getNumericFontHeight
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.Point
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.*
import java.awt.image.BufferedImage
import kotlin.math.absoluteValue
import kotlin.math.ceil
import kotlin.math.floor

/**
 * Usage for compiling:
 * /snap/bin/kotlinc SimpleAirspeedColumn.kt -d ../SimpleAirspeedColumn.jar -cp '.:../../target/aviationHorizonDisplay-1.0-SNAPSHOT.jar' -no-stdlib
 */

class SimpleAirspeedColumn : ScreenElementPlugin {
    override val pluginName: String
        get() = "SimpleAirspeedColumn"
    override val pluginDescription: String
        get() = ""
    override val pluginVersion: String
        get() = "1.0.0"
    override val pluginAuthor: String
        get() = "Samuel-Anton Jansen"
    override val screenElementType: ScreenElementType
        get() = ScreenElementType.AIRSPEED_COLUMN

    override var left: Int = 60
    override var top: Int = 0
    override var width: Int = 100
    override var height: Int = 500

    override var previewImageSource: String? = "./SimpleAirspeedColumn.png"
    override var previewBufferedImage: BufferedImage? = null

    private val airspeedIntervalSeperationFactor = 6
    private val airspeedColoredSectorWidth = 8

    override fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        val airspeedPredicionBar = drawAirspeedPredictionBar(flightData)

        top = (screenProperties.height / 2.0 - height / 2.0).toInt()
        width = 100 + airspeedPredicionBar.width

        val (bufferedImage, g2: Graphics2D) = createGraphics(width, height)
        g2.stroke = BasicStroke(1F)

        drawImageInYCenter(g2, bufferedImage.height, airspeedPredicionBar, bufferedImage.width - airspeedPredicionBar.width)

        val borderRectangle =
            Rectangle(0, 0, bufferedImage.width - airspeedPredicionBar.width - 1, bufferedImage.height - 1)
        g2.color = Color(0, 0, 0, 20)
        g2.fill(borderRectangle)
        g2.color = Color.WHITE
        g2.draw(borderRectangle)

        val airspeedColoredSections = drawAirspeedColoredSections(flightData, aircraftData)
        g2.drawImage(
            airspeedColoredSections,
            null,
            bufferedImage.width - airspeedPredicionBar.width - airspeedColoredSections.width - 1,
            1
        )

        g2.drawImage(drawAirspeedIntervals(flightData), null, 0, 0)

        val airspeedCurrentValueBufferedImage = drawAirspeedCurrentValue(flightData)
        drawImageInYCenter(
            g2,
            bufferedImage.height,
            airspeedCurrentValueBufferedImage,
            bufferedImage.width - airspeedPredicionBar.width - airspeedCurrentValueBufferedImage.width - 2
        )

        g2.dispose()
        return bufferedImage
    }


    private fun drawAirspeedCurrentValue(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(78, 56)
        g2.stroke = BasicStroke(1F)
        g2.font = Font("Dialog", Font.PLAIN, 24)
        val fontMetrics: FontMetrics = g2.fontMetrics

        val textMarginRight = 7
        val leastSignificantNumberHeight = getNumericFontHeight(fontMetrics) + 8
        val rightArrowWidth = airspeedColoredSectorWidth
        val currentSpeedBackgroundHeight: Int = (getNumericFontHeight(fontMetrics) + 18).toInt()

        val currentSpeedBackgroundTopPoint: Int = bufferedImage.height / 2 - currentSpeedBackgroundHeight / 2
        val leastSignificantNumberBackgroundLeftPoint: Int =
            bufferedImage.width - airspeedColoredSectorWidth - textMarginRight - fontMetrics.stringWidth("0") - 3

        val currentSpeedBackgroundPolygon = Polygon()
        currentSpeedBackgroundPolygon.addPoint(0, currentSpeedBackgroundTopPoint)
        currentSpeedBackgroundPolygon.addPoint(leastSignificantNumberBackgroundLeftPoint, currentSpeedBackgroundTopPoint)
        currentSpeedBackgroundPolygon.addPoint(leastSignificantNumberBackgroundLeftPoint, 0)
        currentSpeedBackgroundPolygon.addPoint(bufferedImage.width - rightArrowWidth, 0)
        currentSpeedBackgroundPolygon.addPoint(
            bufferedImage.width - rightArrowWidth,
            (currentSpeedBackgroundTopPoint + currentSpeedBackgroundHeight * 0.30).toInt()
        )
        currentSpeedBackgroundPolygon.addPoint(
            bufferedImage.width,
            (currentSpeedBackgroundTopPoint + currentSpeedBackgroundHeight * 0.5).toInt()
        )
        currentSpeedBackgroundPolygon.addPoint(
            bufferedImage.width - rightArrowWidth,
            (currentSpeedBackgroundTopPoint + currentSpeedBackgroundHeight * 0.70).toInt()
        )
        currentSpeedBackgroundPolygon.addPoint(bufferedImage.width - rightArrowWidth, bufferedImage.height)
        currentSpeedBackgroundPolygon.addPoint(leastSignificantNumberBackgroundLeftPoint, bufferedImage.height)
        currentSpeedBackgroundPolygon.addPoint(
            leastSignificantNumberBackgroundLeftPoint,
            currentSpeedBackgroundTopPoint + currentSpeedBackgroundHeight
        )
        currentSpeedBackgroundPolygon.addPoint(0, currentSpeedBackgroundTopPoint + currentSpeedBackgroundHeight)

        g2.color = Color.BLACK
        g2.fill(currentSpeedBackgroundPolygon)

        val currentAirspeedDec = (flightData.airspeed / 10).toInt()
        val textPositionTop = bufferedImage.height / 2 + getNumericFontHeight(fontMetrics) / 2
        var textPositionLeft =
            bufferedImage.width - airspeedColoredSectorWidth - textMarginRight - fontMetrics.stringWidth(currentAirspeedDec.toString()) - fontMetrics.stringWidth(
                "0"
            )
        if (textPositionLeft < 0) {
            textPositionLeft = 0
        }

        if (currentAirspeedDec != 0) {
            g2.color = Color.WHITE
            g2.drawString(currentAirspeedDec.toString(), textPositionLeft, textPositionTop.toInt())
        }

        val leastSignificantNumber: Int = (floor(flightData.airspeed) % 10).toInt()
        val leastSignificantNumberUpperUpper: Int = (leastSignificantNumber + 2) % 10
        val leastSignificantNumberUpper: Int = (leastSignificantNumber + 1) % 10
        val leastSignificantNumberLower: Int = (leastSignificantNumber - 1) % 10
        val leastSignificantFraction: Double = flightData.airspeed % 1

        val leastSignificantNumberLeftPoint =
            bufferedImage.width - airspeedColoredSectorWidth - textMarginRight - fontMetrics.stringWidth("0") + 2

        g2.color = Color.WHITE
        g2.drawString(
            leastSignificantNumberUpperUpper.toString(),
            leastSignificantNumberLeftPoint,
            (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction - 2)).toInt()
        )
        g2.drawString(
            leastSignificantNumberUpper.toString(),
            leastSignificantNumberLeftPoint,
            (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction - 1)).toInt()
        )

        g2.drawString(
            leastSignificantNumber.toString(),
            leastSignificantNumberLeftPoint,
            (textPositionTop + leastSignificantNumberHeight * leastSignificantFraction).toInt()
        )

        if (leastSignificantNumberLower >= 0) {
            g2.drawString(
                leastSignificantNumberLower.toString(),
                leastSignificantNumberLeftPoint,
                (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction + 1)).toInt()
            )
        }

        g2.dispose()
        return bufferedImage
    }

    private fun drawAirspeedColoredSections(flightData: FlightData, aircraftData: AircraftData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(airspeedColoredSectorWidth, 500 - 2)

        val belowMinAirspeedRectangle = getAirspeedColoredSectionRectangle(
            bufferedImage.width, bufferedImage.height,
            0.0, aircraftData.minAirspeed, flightData.airspeed
        )
        g2.color = Color.RED
        g2.fill(belowMinAirspeedRectangle)

        val minAirspeedRectangle = getAirspeedColoredSectionRectangle(
            bufferedImage.width, bufferedImage.height,
            aircraftData.minAirspeed, aircraftData.minAirspeedNoFlaps, flightData.airspeed
        )
        g2.color = Color.WHITE
        g2.fill(minAirspeedRectangle)

        val minAirspeedNoFlapsRectangle = getAirspeedColoredSectionRectangle(
            bufferedImage.width, bufferedImage.height,
            aircraftData.minAirspeedNoFlaps, aircraftData.maxAirspeedWarningSpeed, flightData.airspeed
        )
        g2.color = Color(0, 168, 0)
        g2.fill(minAirspeedNoFlapsRectangle)

        val maxAirspeedWarningRectangle = getAirspeedColoredSectionRectangle(
            bufferedImage.width, bufferedImage.height,
            aircraftData.maxAirspeedWarningSpeed, aircraftData.maxAirspeed, flightData.airspeed
        )
        g2.color = Color.YELLOW
        g2.fill(maxAirspeedWarningRectangle)

        val maxAirspeedRectangle = Rectangle(
            0,
            0,
            bufferedImage.width,
            (bufferedImage.height / 2 - (aircraftData.maxAirspeed - flightData.airspeed) * airspeedIntervalSeperationFactor).toInt()
        )
        g2.color = Color.RED
        g2.fill(maxAirspeedRectangle)

        g2.dispose()
        return bufferedImage
    }

    private fun getAirspeedColoredSectionRectangle(
        width: Int,
        height: Int,
        lowerSpeedBound: Double,
        upperSpeedBound: Double,
        currentSpeed: Double
    ): Rectangle {
        return Rectangle(
            0, (height / 2.0 - (upperSpeedBound - currentSpeed) * airspeedIntervalSeperationFactor).toInt(),
            width, ((upperSpeedBound - lowerSpeedBound) * airspeedIntervalSeperationFactor).toInt()
        )
    }

    private fun drawAirspeedIntervals(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(100, 500)
        g2.stroke = BasicStroke(1F)
        g2.font = Font("Dialog", Font.PLAIN, 18)
        val fontMetrics: FontMetrics = g2.fontMetrics

        val textMarginRight = 20
        val lineMarginRight = 15
        val airspeedDifference =
            ceil(bufferedImage.height / airspeedIntervalSeperationFactor / 2.0) + airspeedIntervalSeperationFactor

        val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)

        val minAirspeed: Int = (flightData.airspeed - airspeedDifference).toInt()
        val maxAirspeed: Int = (flightData.airspeed + airspeedDifference).toInt()
        for (airspeed in minAirspeed until maxAirspeed) {
            if (airspeed < 0) {
                continue
            } else if (airspeed % 5 != 0) {
                continue
            }

            val airspeedCenterPositionY =
                centerPoint.y + (flightData.airspeed - airspeed) * airspeedIntervalSeperationFactor

            if (airspeed % 10 != 0) {
                g2.drawLine(
                    bufferedImage.width - airspeedColoredSectorWidth - 1, airspeedCenterPositionY.toInt(),
                    bufferedImage.width, airspeedCenterPositionY.toInt()
                )
                continue
            }

            g2.drawLine(
                bufferedImage.width - lineMarginRight, airspeedCenterPositionY.toInt(),
                bufferedImage.width, airspeedCenterPositionY.toInt()
            )

            val textPositionTop = airspeedCenterPositionY + getNumericFontHeight(fontMetrics) / 2
            val textPositionLeft = bufferedImage.width - textMarginRight - fontMetrics.stringWidth(airspeed.toString())

            g2.color = Color.WHITE
            g2.drawString(airspeed.toString(), textPositionLeft, textPositionTop.toInt())
        }

        g2.dispose()
        return bufferedImage
    }

    private fun drawAirspeedPredictionBar(flightData: FlightData): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(4, 500)
        g2.stroke = BasicStroke(1F)
        g2.color = Color.PINK

        val predictionRectangle = if (flightData.airspeedIncreasePrediction > 0) {
            Rectangle(
                0,
                (bufferedImage.height / 2 - flightData.airspeedIncreasePrediction * airspeedIntervalSeperationFactor).toInt(),
                bufferedImage.width,
                (flightData.airspeedIncreasePrediction * airspeedIntervalSeperationFactor).toInt()
            )
        } else {
            Rectangle(
                0, bufferedImage.height / 2,
                bufferedImage.width,
                (flightData.airspeedIncreasePrediction * airspeedIntervalSeperationFactor).toInt().absoluteValue
            )
        }

        g2.fill(predictionRectangle)

        g2.dispose()
        return bufferedImage
    }
}