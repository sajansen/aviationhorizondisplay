package nl.sajansen

import nl.sajansen.objects.AverageList
import org.junit.Test
import kotlin.test.assertEquals

class AverageListTest {

    @Test
    fun `AverageList can give average value`(){
        // Given
        val averageList = AverageList(5)

        // When, then
        assertEquals(5.0, averageList.addAndAverage(5.0))
        assertEquals(5.0, averageList.addAndAverage(5.0))
        assertEquals(20 / 3.0, averageList.addAndAverage(10.0))
        assertEquals(35 / 4.0, averageList.addAndAverage(15.0))
        assertEquals(36 / 5.0, averageList.addAndAverage(1.0))
        assertEquals(32 / 5.0, averageList.addAndAverage(1.0))
        assertEquals(28 / 5.0, averageList.addAndAverage(1.0))
    }
}
