package nl.sajansen.graphics

import nl.sajansen.objects.FlightData
import java.awt.*
import java.awt.image.BufferedImage
import kotlin.math.absoluteValue
import kotlin.math.ceil
import kotlin.math.floor

private const val altitudeIntervalSeperationFactor = 0.7

fun drawAltitude(flightData: FlightData): BufferedImage {
    val (bufferedImage, g2: Graphics2D) = createGraphics(100, 500)
    g2.stroke = BasicStroke(1F)

    val borderRectangle = Rectangle(0, 0, bufferedImage.width - 1, bufferedImage.height - 1)
    g2.color = Color(0, 0, 0, 20)
    g2.fill(borderRectangle)
    g2.color = Color.WHITE
    g2.draw(borderRectangle)

    g2.drawImage(drawAltitudeIntervals(flightData), null, 0, 0)

    val altitudePredicionBar = drawAltitudePredictionBar(flightData)
    drawImageInYCenter(g2, bufferedImage.height, altitudePredicionBar, 1)

    val altitudeCurrentValueBufferedImage = drawAltitudeCurrentValue(flightData)
    drawImageInYCenter(
        g2,
        bufferedImage.height,
        altitudeCurrentValueBufferedImage,
        bufferedImage.width - altitudeCurrentValueBufferedImage.width - 2
    )

    g2.dispose()
    return bufferedImage
}

private fun drawAltitudeCurrentValue(flightData: FlightData): BufferedImage {
    val (bufferedImage, g2: Graphics2D) = createGraphics(96, 56)
    g2.stroke = BasicStroke(1F)
    g2.font = Font("Dialog", Font.PLAIN, 20)
    val fontMetricsSmallNumer: FontMetrics = g2.fontMetrics
    g2.font = Font("Dialog", Font.PLAIN, 24)
    val fontMetricsBigNumber: FontMetrics = g2.fontMetrics

    val textMarginRight = 7
    val leftArrowWidth = 8
    val leastSignificantNumberHeight = getNumericFontHeight(fontMetricsSmallNumer) + 8
    val currentAltitudeBackgroundHeight: Int = (getNumericFontHeight(fontMetricsBigNumber) + 18).toInt()

    val currentAltitudeBackgroundTopPoint: Int = bufferedImage.height / 2 - currentAltitudeBackgroundHeight / 2
    val leastSignificantNumberBackgroundLeftPoint: Int =
        bufferedImage.width - textMarginRight - fontMetricsSmallNumer.stringWidth("00") - 5

    val currentAltitudeBackgroundPolygon = Polygon()
    currentAltitudeBackgroundPolygon.addPoint(leftArrowWidth, currentAltitudeBackgroundTopPoint)
    currentAltitudeBackgroundPolygon.addPoint(
        leastSignificantNumberBackgroundLeftPoint,
        currentAltitudeBackgroundTopPoint
    )
    currentAltitudeBackgroundPolygon.addPoint(leastSignificantNumberBackgroundLeftPoint, 0)
    currentAltitudeBackgroundPolygon.addPoint(bufferedImage.width, 0)
    currentAltitudeBackgroundPolygon.addPoint(bufferedImage.width, bufferedImage.height)
    currentAltitudeBackgroundPolygon.addPoint(leastSignificantNumberBackgroundLeftPoint, bufferedImage.height)
    currentAltitudeBackgroundPolygon.addPoint(
        leastSignificantNumberBackgroundLeftPoint,
        currentAltitudeBackgroundTopPoint + currentAltitudeBackgroundHeight
    )
    currentAltitudeBackgroundPolygon.addPoint(
        leftArrowWidth,
        currentAltitudeBackgroundTopPoint + currentAltitudeBackgroundHeight
    )

    currentAltitudeBackgroundPolygon.addPoint(
        leftArrowWidth,
        (currentAltitudeBackgroundTopPoint + currentAltitudeBackgroundHeight * 0.70).toInt()
    )
    currentAltitudeBackgroundPolygon.addPoint(
        0,
        (currentAltitudeBackgroundTopPoint + currentAltitudeBackgroundHeight * 0.5).toInt()
    )
    currentAltitudeBackgroundPolygon.addPoint(
        leftArrowWidth,
        (currentAltitudeBackgroundTopPoint + currentAltitudeBackgroundHeight * 0.30).toInt()
    )

    g2.color = Color.BLACK
    g2.fill(currentAltitudeBackgroundPolygon)

    val currentAltitudeHec = (flightData.altitude / 100).toInt()
    var currentAltitudeHecString = currentAltitudeHec.toString()
    if (currentAltitudeHec == 0 && flightData.altitude < 0) {
        currentAltitudeHecString = "-0"
    }

    val textPositionTop = bufferedImage.height / 2 + getNumericFontHeight(fontMetricsBigNumber) / 2
    var textPositionLeft =
        bufferedImage.width - textMarginRight -
                fontMetricsBigNumber.stringWidth(currentAltitudeHecString) -
                fontMetricsSmallNumer.stringWidth("00") - 1
    if (textPositionLeft < 0) {
        textPositionLeft = 0
    }

    g2.color = Color.WHITE
    g2.drawString(currentAltitudeHecString, textPositionLeft, textPositionTop.toInt())

    // Determine if rounding must be done to the upper or lower value depending on the altitude sign/direction
    val leastSignificantNumberRounded =
        if (flightData.altitude > 0) floor(flightData.altitude % 100 / 20.0) else ceil(flightData.altitude % 100 / 20.0)

    val leastSignificantNumber: Int = ((leastSignificantNumberRounded * 20) % 100).toInt()
    val leastSignificantNumberUpperUpper: Int = (leastSignificantNumber + 40) % 100
    val leastSignificantNumberUpper: Int = (leastSignificantNumber + 20) % 100
    val leastSignificantNumberLower: Int = (leastSignificantNumber - 20) % 100
    val leastSignificantNumberLowerLower: Int = (leastSignificantNumber - 40) % 100

    val leastSignificantFraction: Double = flightData.altitude % 100 % 20 / 20.0

    val leastSignificantNumberLeftPoint =
        bufferedImage.width - textMarginRight - fontMetricsSmallNumer.stringWidth("00") + 2

    g2.font = Font("Dialog", Font.PLAIN, 20)
    g2.color = Color.WHITE
    g2.drawString(
        String.format("%02d", leastSignificantNumberUpperUpper.absoluteValue),
        leastSignificantNumberLeftPoint,
        (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction - 2)).toInt()
    )
    g2.drawString(
        String.format("%02d", leastSignificantNumberUpper.absoluteValue),
        leastSignificantNumberLeftPoint,
        (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction - 1)).toInt()
    )

    g2.drawString(
        String.format("%02d", leastSignificantNumber.absoluteValue),
        leastSignificantNumberLeftPoint,
        (textPositionTop + leastSignificantNumberHeight * leastSignificantFraction).toInt()
    )

    g2.drawString(
        String.format("%02d", leastSignificantNumberLower.absoluteValue),
        leastSignificantNumberLeftPoint,
        (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction + 1)).toInt()
    )
    g2.drawString(
        String.format("%02d", leastSignificantNumberLowerLower.absoluteValue),
        leastSignificantNumberLeftPoint,
        (textPositionTop + leastSignificantNumberHeight * (leastSignificantFraction + 2)).toInt()
    )

    g2.dispose()
    return bufferedImage
}

private fun drawAltitudeIntervals(flightData: FlightData): BufferedImage {
    val (bufferedImage, g2: Graphics2D) = createGraphics(100, 500)
    g2.stroke = BasicStroke(1F)
    g2.font = Font("Dialog", Font.PLAIN, 18)
    val fontMetrics: FontMetrics = g2.fontMetrics

    val textMarginLeft = 20
    val lineSmallSize = 7
    val lineLargeSize = 13
    val altitudeDifference =
        ceil(bufferedImage.height / altitudeIntervalSeperationFactor / 2.0) + altitudeIntervalSeperationFactor

    val centerPoint = Point(bufferedImage.width / 2, bufferedImage.height / 2)

    val minAltitude: Int = (flightData.altitude - altitudeDifference).toInt()
    val maxAltitude: Int = (flightData.altitude + altitudeDifference).toInt()
    for (altitude in minAltitude until maxAltitude) {
        if (altitude % 20 != 0) {
            continue
        }

        val altitudeCenterPositionY =
            centerPoint.y + (flightData.altitude - altitude) * altitudeIntervalSeperationFactor

        if (altitude % 100 != 0) {
            g2.drawLine(
                0, altitudeCenterPositionY.toInt(),
                lineSmallSize, altitudeCenterPositionY.toInt()
            )
            continue
        }

        g2.drawLine(
            0, altitudeCenterPositionY.toInt(),
            lineLargeSize, altitudeCenterPositionY.toInt()
        )

        val textPositionTop = altitudeCenterPositionY + getNumericFontHeight(fontMetrics) / 2

        g2.color = Color.WHITE
        g2.drawString(altitude.toString(), textMarginLeft, textPositionTop.toInt())
    }

    g2.dispose()
    return bufferedImage
}

private fun drawAltitudePredictionBar(flightData: FlightData): BufferedImage {
    val (bufferedImage, g2: Graphics2D) = createGraphics(5, 500)
    g2.stroke = BasicStroke(1F)
    g2.color = Color.PINK

    val predictionRectangle = if (flightData.altitudeIncreasePrediction > 0) {
        Rectangle(
            0,
            (bufferedImage.height / 2 - flightData.altitudeIncreasePrediction * altitudeIntervalSeperationFactor).toInt(),
            bufferedImage.width,
            (flightData.altitudeIncreasePrediction * altitudeIntervalSeperationFactor).toInt()
        )
    } else {
        Rectangle(
            0, bufferedImage.height / 2,
            bufferedImage.width,
            (flightData.altitudeIncreasePrediction * altitudeIntervalSeperationFactor).toInt().absoluteValue
        )
    }

    g2.fill(predictionRectangle)

    g2.dispose()
    return bufferedImage
}