package nl.sajansen.screens

import nl.sajansen.config.Config
import nl.sajansen.graphics.createGraphics
import nl.sajansen.graphics.drawImageInXYCenter
import nl.sajansen.graphics.setDefaultRenderingHints
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.plugins.PluginLoader
import nl.sajansen.plugins.ScreenElementPlugin
import nl.sajansen.plugins.ScreenElementType
import java.awt.*
import java.awt.image.BufferedImage
import javax.swing.JPanel


class MainScreen(private val aircraftData: AircraftData) : JPanel() {

    private val screenProperties = ScreenProperties(
        Config.MAIN_SCREEN_WIDTH,
        Config.MAIN_SCREEN_HEIGHT,
        "MainScreen",
        Config.MAIN_SCREEN_PITCH_TO_SCREEN_FACTOR
    )

    var flightData: FlightData = FlightData()
    var showMenu: Boolean = false
    val menuItems = ArrayList<ArrayList<String>>()
    private var isRepainting = false

    init {
        preferredSize = Dimension(screenProperties.width, screenProperties.height)
    }

    fun isRepainting(): Boolean {
        return isRepainting
    }

    public override fun paintComponent(g: Graphics) {
        isRepainting = true

        super.paintComponent(g)

        val g2 = g as Graphics2D
        setDefaultRenderingHints(g2)

        val horizonPlugin = PluginLoader.getAllEnabledScreenElementPlugins()
            .firstOrNull { plugin -> plugin.screenElementType.equals(ScreenElementType.HORIZON) }
        if (horizonPlugin != null) {
            drawScreenElementPluginGraphics(horizonPlugin, g2)
        }

        val attitudeLinesPlugin = PluginLoader.getAllEnabledScreenElementPlugins()
            .firstOrNull { plugin -> plugin.screenElementType.equals(ScreenElementType.ATTITUDE_LINES) }
        if (attitudeLinesPlugin != null) {
            drawScreenElementPluginGraphics(attitudeLinesPlugin, g2)
        }

        for (plugin in PluginLoader.getAllEnabledScreenElementPlugins()
            .filter { plugin ->
                !plugin.screenElementType.equals(ScreenElementType.HORIZON)
                        && !plugin.screenElementType.equals(ScreenElementType.ATTITUDE_LINES)
            }) {
            drawScreenElementPluginGraphics(plugin, g2)
        }

        if (showMenu) {
            drawImageInXYCenter(g2, screenProperties.width, screenProperties.height, drawMenu())
        }

        isRepainting = false
    }

    private fun drawScreenElementPluginGraphics(plugin: ScreenElementPlugin, g2: Graphics2D) {
        val bufferedImagePlugin = plugin.getBufferedImage(flightData, aircraftData, screenProperties) ?: return
        g2.drawImage(bufferedImagePlugin, null, plugin.left, plugin.top)
    }

    fun drawMenu(): BufferedImage {
        val (bufferedImage, g2: Graphics2D) = createGraphics(300, 200)
        g2.stroke = BasicStroke(1F)

        val marginTop = 50
        val menuItemMarginTop = 10
        val keycodeMarginLeft = 30
        val detailsMarginLeft = 150

        g2.color = Color(0, 0, 0, 100)
        g2.fillRect(0, 0, bufferedImage.width, bufferedImage.height)

        g2.font = Font("Dialog", Font.PLAIN, 28)
        var fontMetrics: FontMetrics = g2.fontMetrics
        g2.color = Color.BLACK
        g2.drawString(
            "MENU",
            bufferedImage.width / 2 - fontMetrics.stringWidth("MENU") / 2 + 3,
            fontMetrics.height + 5 + 2
        )
        g2.color = Color.WHITE
        g2.drawString("MENU", bufferedImage.width / 2 - fontMetrics.stringWidth("MENU") / 2, fontMetrics.height + 5)

        g2.font = Font("Dialog", Font.PLAIN, 18)
        fontMetrics = g2.fontMetrics
        for ((index, item) in menuItems.withIndex()) {
            val menuItemPositionTop = marginTop + (fontMetrics.height + menuItemMarginTop) * (index + 1)

            g2.color = Color.BLACK
            g2.drawString(item.get(0), keycodeMarginLeft + 2, menuItemPositionTop + 1)
            g2.drawString(item.get(1), detailsMarginLeft + 2, menuItemPositionTop + 1)

            g2.color = Color.WHITE
            g2.drawString(item.get(0), keycodeMarginLeft, menuItemPositionTop)
            g2.drawString(item.get(1), detailsMarginLeft, menuItemPositionTop)
        }

        g2.dispose()
        return bufferedImage
    }
}