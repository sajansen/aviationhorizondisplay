package nl.sajansen.flightdata

import nl.sajansen.objects.FlightData

interface FlightDataInterface {
    fun setup(interval: Long)
    fun retrieveFlightData(): FlightData
    fun setFlightDataUpdateInterval(interval: Long) {}
}