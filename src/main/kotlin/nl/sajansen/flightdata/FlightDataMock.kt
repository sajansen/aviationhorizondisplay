package nl.sajansen.flightdata

import nl.sajansen.objects.AverageList
import nl.sajansen.objects.FlightData
import java.util.*

class FlightDataMock: FlightDataInterface {
    private var flightData = FlightData()
    private var flightDataUpdateInterval: Long = 20

    private var pitchDirection = 1.0
    private var bankDirection = 1.0
    private var headingDirection = 1.0
    private var airspeedDirection = 1.0
    private var altitudeDirection = 1.0

    private var pitchIncrease = 0.0
    private var bankIncrease = 0.0
    private var headingIncrease = 0.0
    private var airspeedIncrease = 0.0
    private var altitudeIncrease = 0.0

    private val averageSampleTime = 2
    private var altitudeAverageList = AverageList((1000 / flightDataUpdateInterval * averageSampleTime).toInt())

    override fun setup(interval: Long) {
        flightDataUpdateInterval = interval
    }

    fun calculateNewIncrease(
        currentAbsoluteValue: Double,
        maxAbsoluteValue: Double,
        currentIncreaseValue: Double,
        maxIncreaseValue: Double,
        increaseDirection: Double,
        accelerationValue: Double
    ): Pair<Double, Double> {
        return calculateNewIncrease(
            currentAbsoluteValue,
            maxAbsoluteValue,
            -1 * maxAbsoluteValue,
            currentIncreaseValue,
            maxIncreaseValue,
            increaseDirection,
            accelerationValue
        )
    }

    fun calculateNewIncrease(
        currentAbsoluteValue: Double,
        maxAbsoluteValue: Double,
        minAbsoluteValue: Double,
        currentIncreaseValue: Double,
        maxIncreaseValue: Double,
        increaseDirection: Double,
        accelerationValue: Double
    ): Pair<Double, Double> {
        var newIncreaseDirection = increaseDirection
        if (currentAbsoluteValue > maxAbsoluteValue) {
            newIncreaseDirection = -1.0
        } else if (currentAbsoluteValue < minAbsoluteValue) {
            newIncreaseDirection = 1.0
        }

        var newIncreaseValue = currentIncreaseValue
        newIncreaseValue += newIncreaseDirection * accelerationValue
        if (newIncreaseValue > maxIncreaseValue) {
            newIncreaseValue = maxIncreaseValue
        } else if (newIncreaseValue < -1 * maxIncreaseValue) {
            newIncreaseValue = -1 * maxIncreaseValue
        }

        return Pair(newIncreaseValue, newIncreaseDirection)
    }

    override fun retrieveFlightData(): FlightData {
        val newFlightData = FlightData()

        newFlightData.startDate = flightData.startDate

        var result =
            calculateNewIncrease(flightData.pitch.getDegrees(), 10.0, pitchIncrease, 0.2, pitchDirection, 0.002)
        pitchIncrease = result.first
        pitchDirection = result.second
        newFlightData.pitch.setDegrees(flightData.pitch.getDegrees() + pitchIncrease)

        result = calculateNewIncrease(flightData.bank.getDegrees(), 20.0, bankIncrease, 0.125, bankDirection, 0.002)
        bankIncrease = result.first
        bankDirection = result.second
        newFlightData.bank.setDegrees(flightData.bank.getDegrees() + bankIncrease)

        result = calculateNewIncrease(flightData.heading.getDegrees(), 40.0, 20.0, headingIncrease, 0.25, headingDirection, 0.002)
        headingIncrease = result.first
        headingDirection = result.second
        newFlightData.heading.setDegrees(flightData.heading.getDegrees() + headingIncrease)
        while (newFlightData.heading.getDegrees() < 0) {
            newFlightData.heading.setDegrees(360 + newFlightData.heading.getDegrees())
        }

        result =
            calculateNewIncrease(
                flightData.airspeed,
                110.0,
                90.0,
                airspeedIncrease,
                0.07,
                airspeedDirection,
                Math.random() / 1000
            )
        airspeedIncrease = result.first
        airspeedDirection = result.second
        newFlightData.airspeed = flightData.airspeed + airspeedIncrease
        if (newFlightData.airspeed < 0.0) {
            newFlightData.airspeed = 0.0
        }

        result = calculateNewIncrease(
            flightData.altitude,
            7001.0,
            6000.0,
            altitudeIncrease,
            1.5 + Math.random(),
            altitudeDirection,
            Math.random() / 1
        )
        altitudeIncrease = result.first
        altitudeDirection = result.second
        newFlightData.altitude = flightData.altitude + altitudeIncrease


        // Averages
        newFlightData.bankIncreasePrediction.setDegrees((newFlightData.bank.getDegrees() - flightData.bank.getDegrees()) * 1000 / flightDataUpdateInterval)
        newFlightData.pitchIncreasePrediction.setDegrees((newFlightData.pitch.getDegrees() - flightData.pitch.getDegrees()) * 1000 / flightDataUpdateInterval)
        newFlightData.headingIncreasePrediction.setDegrees((newFlightData.heading.getDegrees() - flightData.heading.getDegrees()) * 1000 / flightDataUpdateInterval)
        newFlightData.airspeedIncreasePrediction = (newFlightData.airspeed - flightData.airspeed) * 1000 / flightDataUpdateInterval
        newFlightData.altitudeIncreasePrediction = altitudeAverageList.addAndAverage(newFlightData.altitude - flightData.altitude) * 1000 / flightDataUpdateInterval

        newFlightData.lastUpdateDate = Date()
        flightData = newFlightData
        return newFlightData
    }

    override fun setFlightDataUpdateInterval(interval: Long) {
        flightDataUpdateInterval = interval
    }
}