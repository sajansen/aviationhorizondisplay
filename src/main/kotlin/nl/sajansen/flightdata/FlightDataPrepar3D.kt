package nl.sajansen.flightdata

import com.flightsim.fsuipc.FSAircraft
import com.flightsim.fsuipc.fsuipc_wrapper
import nl.sajansen.objects.AverageList
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.FlightSimError
import java.util.*

class FlightDataPrepar3D : FlightDataInterface {
    private var wrapper: Int = 0
    private var flightData = FlightData()
    private var flightDataUpdateInterval: Long = 200

    private val averageSampleTime = 1
    private var airspeedAverageList = AverageList((1000 / flightDataUpdateInterval * averageSampleTime).toInt())
    private var headingAverageList = AverageList((1000 / flightDataUpdateInterval * averageSampleTime).toInt())
    private var bankAverageList = AverageList((1000 / flightDataUpdateInterval * averageSampleTime).toInt())

    override fun setup(interval: Long) {
        flightDataUpdateInterval = interval

        wrapper = fsuipc_wrapper.Open(fsuipc_wrapper.SIM_ANY)

        if (wrapper == 0) {
            throw FlightSimError("FLight simulator not found")
        }
    }

    override fun retrieveFlightData(): FlightData {
        val newFlightData = FlightData()
        newFlightData.startDate = flightData.startDate

        val fsAircraft = FSAircraft()

        newFlightData.airspeed = fsAircraft.IAS() / 128.0
        newFlightData.altitude = fsAircraft.Altitude() * 3.28084
        newFlightData.verticalSpeed = fsAircraft.VerticalSpeed() * 60 * 3.28084 / 256 // In meters
        newFlightData.heading.setDegrees((360 + (fsAircraft.Heading() + fsAircraft.Magnetic() * (360 + 160)) * 360.0 / (65536.0 * 65536.0)) % 360)
        newFlightData.pitch.setDegrees(-1 * fsAircraft.Pitch())
        newFlightData.bank.setDegrees(fsAircraft.Bank())
        newFlightData.turnCoordinator = fsAircraft.getFloat(0x0380).toDouble()

        // Predictions
        newFlightData.bankIncreasePrediction.setDegrees(
            bankAverageList.addAndAverage(newFlightData.bank.getDegrees() - flightData.bank.getDegrees()) * 1000 / flightDataUpdateInterval)
        newFlightData.headingIncreasePrediction.setDegrees(headingAverageList.addAndAverage(newFlightData.heading.getDegrees() - flightData.heading.getDegrees()) * 1000 / flightDataUpdateInterval * 2)
        newFlightData.airspeedIncreasePrediction = airspeedAverageList.addAndAverage(newFlightData.airspeed - flightData.airspeed) * 1000 / flightDataUpdateInterval * 2
        newFlightData.altitudeIncreasePrediction = newFlightData.verticalSpeed / 10

        newFlightData.lastUpdateDate = Date()
        flightData = newFlightData
        return newFlightData
    }

    override fun setFlightDataUpdateInterval(interval: Long) {
        flightDataUpdateInterval = interval
    }
}