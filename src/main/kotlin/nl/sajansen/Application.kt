package nl.sajansen

import nl.sajansen.config.Config
import nl.sajansen.flightdata.FlightDataMock
import nl.sajansen.flightdata.FlightDataPrepar3D
import nl.sajansen.objects.FlightSimError
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.objects.aircrafts.MooneyAcclaim
import nl.sajansen.plugins.PluginLoader
import nl.sajansen.windows.MainWindow
import java.awt.EventQueue
import java.util.logging.Logger


private fun createAndShowGUI() {
    val logger = Logger.getLogger("Application")

    val aircraft: AircraftData = PluginLoader.getAllEnabledAircraftPlugins().firstOrNull() ?: MooneyAcclaim()

    var frame: MainWindow
    try {
        frame = MainWindow(
            aircraft,
            FlightDataPrepar3D()
        )
    } catch (e: Error) {
        if (e is UnsatisfiedLinkError || e is NoClassDefFoundError || e is FlightSimError) {
            logger.warning("Failed to connect to the Flight Simulator: $e")
            logger.info("Falling back to FlightDataMock")
            frame = MainWindow(
                aircraft,
                FlightDataMock()
            )
        } else {
            throw e
        }
    }

    frame.isVisible = true

    frame.startFlightDataUpdateTimer()
    frame.startScreenUpdateTimer()
}

fun main(args: Array<String>) {
    Config.load()

    PluginLoader.loadAll()
    PluginLoader.enableAllConfigEnabledPlugins()
    PluginLoader.setupAll()

    EventQueue.invokeLater(::createAndShowGUI)
}
