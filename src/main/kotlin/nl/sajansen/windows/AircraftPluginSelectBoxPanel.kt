package nl.sajansen.windows

import nl.sajansen.config.Config
import nl.sajansen.graphics.setDefaultRenderingHints
import nl.sajansen.plugins.AircraftPlugin
import nl.sajansen.plugins.PluginLoader
import java.awt.*
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import javax.imageio.ImageIO
import javax.swing.JPanel

class AircraftPluginSelectBoxMouseListener(private val parent: JPanel, private val plugin: AircraftPlugin) : MouseListener {
    override fun mouseReleased(p0: MouseEvent?) {}

    override fun mouseEntered(p0: MouseEvent?) {}

    override fun mouseClicked(p0: MouseEvent?) {
        if (PluginLoader.isEnabled(plugin)) {
            PluginLoader.disable(plugin)
        } else {
            PluginLoader.enable(plugin)
        }

        Config.save()
        parent.parent.repaint()
    }

    override fun mouseExited(p0: MouseEvent?) {}

    override fun mousePressed(p0: MouseEvent?) {}
}

class AircraftPluginSelectBoxPanel(private val plugin: AircraftPlugin) : JPanel() {
    private val panelWidth = 300
    private val panelHeight = 150
    private val checkmarkWidth = 150
    private val checkmarkHeight = 150
    private val checkmarkOpacity = 0.7F

    private val textMarginY = 5
    private val textMarginLeft = 20
    private val textMarginRight = 10
    private val backgroundSkew = 15

    init {
        preferredSize = Dimension(panelWidth, panelHeight)
        minimumSize = Dimension(panelWidth, panelHeight)
        maximumSize = Dimension(panelWidth, panelHeight)
        isVisible = true
        cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
        toolTipText = if (plugin.pluginDescription.isNotEmpty()) plugin.pluginDescription else "No description given"

        addMouseListener(AircraftPluginSelectBoxMouseListener(this, plugin))
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)

        val g2 = g as Graphics2D
        setDefaultRenderingHints(g2)

        drawBackgroundImage(g2)
        drawAircraftName(g2)
        drawSelectedCheckmark(g2)
    }

    private fun drawAircraftName(g2: Graphics2D) {
        g2.stroke = BasicStroke(1F)
        g2.font = Font("Dialog", Font.BOLD, 16)
        val fontMetrics = g2.fontMetrics
        val textWidth = fontMetrics.stringWidth(plugin.aircraftName)

        // Text background
        val background = Polygon()
        background.addPoint(0, 0)
        background.addPoint(textMarginLeft + textMarginRight + textWidth + backgroundSkew, 0)
        background.addPoint(textMarginLeft + textMarginRight + textWidth, textMarginY * 2 + fontMetrics.height)
        background.addPoint(0, textMarginY * 2 + fontMetrics.height)
        g2.color = Color(240, 240, 240)
        g2.fill(background)
        g2.color = Color.GRAY
        g2.draw(background)

        // Aircraft name
        g2.color = Color.BLACK
        g2.drawString(plugin.aircraftName, textMarginLeft, (textMarginY + fontMetrics.height * 0.9).toInt())
    }

    private fun drawSelectedCheckmark(g2: Graphics2D) {
        if (!PluginLoader.isEnabled(plugin)) {
            return
        }

        val originalComposite = g2.composite
        g2.composite = AlphaComposite.SrcOver.derive(checkmarkOpacity);

        g2.drawImage(
            ImageIO.read(this::class.java.classLoader.getResource("checkmark_green_520.png")),
            (panelWidth - checkmarkWidth) / 2,
            (panelHeight - checkmarkHeight) / 2,
            checkmarkWidth,
            checkmarkHeight,
            this
        )

        g2.composite = originalComposite
    }

    private fun drawBackgroundImage(g2: Graphics2D) {
        val previewImage = plugin.getPreviewImage()
        if (previewImage != null) {
            g2.drawImage(previewImage, 0, 0, panelWidth, panelHeight, this)
        } else {
            g2.color = Color.BLACK
            g2.fillRect(0, 0, panelWidth, panelHeight)
        }
    }

}