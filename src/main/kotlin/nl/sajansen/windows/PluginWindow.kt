package nl.sajansen.windows

import nl.sajansen.plugins.BasePlugin
import nl.sajansen.plugins.PluginLoader
import java.awt.*
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.util.logging.Logger
import javax.swing.*
import kotlin.math.ceil


private class PluginWindowKeyListener(private val parent: PluginWindow) : KeyListener {
    val logger = Logger.getLogger(PluginWindowKeyListener::class.java.toString())

    override fun keyTyped(keyEvent: KeyEvent?) {}

    override fun keyPressed(keyEvent: KeyEvent?) {
        if (keyEvent == null) return

        if (isCtrlPressedWith(keyEvent, KeyEvent.VK_W)) {
            logger.fine("Exiting PluginWindow")
            parent.dispose()
            return
        }
    }

    override fun keyReleased(keyEvent: KeyEvent?) {
        if (keyEvent == null) return
    }

    private fun isCtrlPressedWith(event: KeyEvent, key: Int): Boolean {
        return event.keyCode == key && (event.modifiers or KeyEvent.CTRL_MASK) == KeyEvent.CTRL_MASK
    }
}

class PluginWindow : JFrame() {

    private val pluginCheckboxes = HashMap<BasePlugin, JCheckBox>()

    init {
        createUI("Plugin Manager")
        isVisible = true

        addKeyListener(PluginWindowKeyListener(this))
    }

    private fun createUI(title: String) {
        setTitle(title)

        defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
        setSize(600, 300)
        setLocation(1300, 200)
        background = Color(0, 5, 100)

        val mainPanel = JPanel()
        mainPanel.layout = BoxLayout(mainPanel, BoxLayout.Y_AXIS)
        mainPanel.background = background

        val screenElementPluginsTableLabel = JLabel("Graphical plugins", JLabel.CENTER)
        screenElementPluginsTableLabel.font = Font("Dialog", Font.BOLD, 16)
        screenElementPluginsTableLabel.foreground = Color.WHITE
        screenElementPluginsTableLabel.alignmentX = Component.CENTER_ALIGNMENT
        mainPanel.add(screenElementPluginsTableLabel)

        mainPanel.add(createScreenElementPluginsTable())

        val aircraftPluginsTableLabel = JLabel("Aircraft plugins", JLabel.CENTER)
        aircraftPluginsTableLabel.font = Font("Dialog", Font.BOLD, 16)
        aircraftPluginsTableLabel.foreground = Color.WHITE
        aircraftPluginsTableLabel.alignmentX = Component.CENTER_ALIGNMENT
        mainPanel.add(aircraftPluginsTableLabel)

        mainPanel.add(createAircraftPluginsTable())

        val scrollPane = JScrollPane(mainPanel)
        add(scrollPane)
        pack()
    }

    private fun createScreenElementPluginsTable(): JPanel {
        val tablePanel = JPanel()
        tablePanel.background = null
        tablePanel.preferredSize = Dimension(
            210 * 3,
            (210 * ceil(PluginLoader.getAllAvailableScreenElementPlugins().size / 3.0)).toInt()
        )
        tablePanel.layout = FlowLayout()

        for (plugin in PluginLoader.getAllAvailableScreenElementPlugins()) {
            tablePanel.add(ScreenElementPluginSelectBoxPanel(plugin))
        }

        return tablePanel
    }

    private fun createAircraftPluginsTable(): JPanel {
        val tablePanel = JPanel()
        tablePanel.background = null
        tablePanel.preferredSize = Dimension(
            310 * 2,
            (160 * ceil(PluginLoader.getAllAvailableAircraftPlugins().size / 2.0)).toInt()
        )
        tablePanel.layout = FlowLayout()

        for (plugin in PluginLoader.getAllAvailableAircraftPlugins()) {
            tablePanel.add(AircraftPluginSelectBoxPanel(plugin))
        }

        return tablePanel
    }

    fun reloadPluginStatusses() {
        for ((plugin, checkbox) in pluginCheckboxes) {
            checkbox.isSelected = PluginLoader.isEnabled(plugin)
        }
    }
}