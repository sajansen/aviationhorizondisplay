package nl.sajansen.windows

import nl.sajansen.config.Config
import nl.sajansen.flightdata.FlightDataInterface
import nl.sajansen.objects.FlightData
import nl.sajansen.objects.aircrafts.AircraftData
import nl.sajansen.screens.MainScreen
import java.awt.Color
import java.awt.EventQueue
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.util.*
import java.util.logging.Logger
import javax.swing.GroupLayout
import javax.swing.JComponent
import javax.swing.JFrame
import kotlin.system.exitProcess

private class MainWindowKeyListener(private val parent: MainWindow) : KeyListener {
    private val logger = Logger.getLogger(MainWindowKeyListener::class.java.toString())

    override fun keyTyped(keyEvent: KeyEvent?) {}

    override fun keyPressed(keyEvent: KeyEvent?) {
        if (keyEvent == null) return

        if (keyEvent.keyCode == KeyEvent.VK_CONTROL) {
            parent.showMenu(true)
        }

        if (isCtrlPressedWith(keyEvent, KeyEvent.VK_W)) {
            logger.fine("Exiting MainWindow")
            parent.showMenu(false)
            parent.dispose()
            exitProcess(0)
        }
        if (isCtrlPressedWith(keyEvent, KeyEvent.VK_P)) {
            parent.showMenu(false)
            EventQueue.invokeLater { PluginWindow() }
            return
        }
    }

    override fun keyReleased(keyEvent: KeyEvent?) {
        if (keyEvent == null) return

        if (keyEvent.keyCode == KeyEvent.VK_CONTROL) {
            parent.showMenu(false)
        }
    }

    private fun isCtrlPressedWith(event: KeyEvent, key: Int): Boolean {
        return event.keyCode == key && event.modifiers == KeyEvent.CTRL_MASK
    }
}

class MainWindow(
    private var aircraftData: AircraftData,
    private val flightDataImpl: FlightDataInterface
) : JFrame() {
    private val logger = Logger.getLogger(MainWindow::class.java.toString())

    private lateinit var mainScreen: MainScreen
    private val flightDataUpdateTimer = Timer()
    private val flightDataUpdateInterval: Long = Config.FLIGHT_DATA_UPDATE_TIMER_INTERVAL
    private val screenUpdateTimer = Timer()
    private val screenUpdateInterval: Long = Config.SCREEN_UPDATE_TIMER_INTERVAL
    private var flightData = FlightData()

    init {
        createUI("AviationHorizonDisplay")
        addKeyListener(MainWindowKeyListener(this))

        mainScreen.menuItems.add(arrayListOf("Ctrl + W", "Quit"))
        mainScreen.menuItems.add(arrayListOf("Ctrl + P", "Plugins"))

        flightDataImpl.setup(flightDataUpdateInterval)

        logger.info("MainWindow loaded with aircraft: ${aircraftData.aircraftName}")
    }

    private fun createUI(title: String) {
        setTitle(title)

        defaultCloseOperation = EXIT_ON_CLOSE
        setSize(300, 200)
        setLocation(500, 150)
        background = Color(0, 5, 100)

        mainScreen = MainScreen(aircraftData)
        createLayout(mainScreen)
    }

    private fun createLayout(vararg arg: JComponent) {

        val gl = GroupLayout(contentPane)
        contentPane.layout = gl

        gl.autoCreateContainerGaps = false

        gl.setHorizontalGroup(
            gl.createSequentialGroup()
                .addComponent(arg[0])
        )

        gl.setVerticalGroup(
            gl.createSequentialGroup()
                .addComponent(arg[0])
        )

        pack()
    }

    fun startFlightDataUpdateTimer() {
        flightData.startDate = Date()

        flightDataUpdateTimer.schedule(object : TimerTask() {
            override fun run() {
                flightData = retrieveFlightData()

                mainScreen.flightData = flightData
            }
        }, Config.FLIGHT_DATA_UPDATE_TIMER_INIT_DELAY, flightDataUpdateInterval)
    }

    fun stopFlightDataUpdateTimer() {
        flightDataUpdateTimer.cancel()
    }

    fun startScreenUpdateTimer() {
        screenUpdateTimer.schedule(object : TimerTask() {
            override fun run() {
                if (mainScreen.isRepainting()) {
                    logger.fine("Skipping mainScreen update: still updating")
                    return
                }

                mainScreen.repaint()
            }
        }, Config.SCREEN_UPDATE_TIMER_INIT_DELAY, screenUpdateInterval)
    }

    fun stopScreenUpdateTimer() {
        screenUpdateTimer.cancel()
    }

    fun retrieveFlightData(): FlightData {
        return flightDataImpl.retrieveFlightData()
    }

    fun showMenu(show: Boolean) {
        mainScreen.showMenu = show
    }
}