package nl.sajansen.plugins

import nl.sajansen.objects.aircrafts.AircraftData
import java.awt.image.BufferedImage
import java.net.URL
import javax.imageio.ImageIO

interface AircraftPlugin : BasePlugin, AircraftData {

    var previewImageSource: String?
    var previewBufferedImage: BufferedImage?

    fun getPreviewImage(): BufferedImage? {
        if (previewBufferedImage == null && previewImageSource != null) {
            val resource: URL = this::class.java.classLoader.getResource(previewImageSource) ?: return null
            previewBufferedImage = ImageIO.read(resource)
        }

        return previewBufferedImage
    }
}