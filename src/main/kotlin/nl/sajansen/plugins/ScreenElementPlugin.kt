package nl.sajansen.plugins

import nl.sajansen.objects.FlightData
import nl.sajansen.objects.ScreenProperties
import nl.sajansen.objects.aircrafts.AircraftData
import java.awt.image.BufferedImage
import java.net.URL
import javax.imageio.ImageIO

enum class ScreenElementType {
    UNKNOWN,
    AIRSPEED_COLUMN,
    ALTITUDE_COLUMN,
    HEADING_COMPASS,
    STATIC_AIRCRAFT_OVERLAY,
    TURN_INDICATOR,
    ATTITUDE_LINES,
    HORIZON
}

interface ScreenElementPlugin : BasePlugin {
    val screenElementType: ScreenElementType

    var left: Int
    var top: Int
    var width: Int
    var height: Int

    var previewImageSource: String?
    var previewBufferedImage: BufferedImage?

    fun getPreviewImage(): BufferedImage? {
        if (previewBufferedImage == null && previewImageSource != null) {
            val resource: URL = this::class.java.classLoader.getResource(previewImageSource) ?: return null
            previewBufferedImage = ImageIO.read(resource)
        }

        return previewBufferedImage
    }

    fun getBufferedImage(
        flightData: FlightData,
        aircraftData: AircraftData,
        screenProperties: ScreenProperties
    ): BufferedImage? {
        return null
    }
}