package nl.sajansen.plugins

interface BasePlugin {
    val pluginName: String
    val pluginDescription: String
    val pluginVersion: String
    val pluginAuthor: String

    fun setup() {
        println("Plugin $pluginName says Hello")
    }
}