package nl.sajansen.plugins

import nl.sajansen.config.Config
import java.io.File
import java.io.InvalidClassException
import java.net.URL
import java.net.URLClassLoader
import java.util.*
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.logging.Logger
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

object PluginLoader {
    private val logger = Logger.getLogger(PluginLoader.toString())
    private val pluginDirectory = Config.PLUGIN_DIRECTORY

    // Available plugins
    private val availableScreenElementPlugins = HashSet<ScreenElementPlugin>()
    private val availableAircraftPlugins = HashSet<AircraftPlugin>()

    // Enabled plugins
    private val enabledScreenElementPlugins = HashSet<ScreenElementPlugin>()
    private val enabledAircraftPlugins = HashSet<AircraftPlugin>()

    fun loadAll() {
        val pluginURLs: Array<URL?>? = getPluginFiles()

        if (pluginURLs == null) {
            logger.info("No plugins found")
            return
        }

        val urlClassLoader = URLClassLoader(pluginURLs, this::class.java.classLoader)

        val classNames = getPluginClassNames(urlClassLoader)
        logger.info("Plugins found: " + classNames.joinToString { it })

        loadClassNames(classNames, urlClassLoader)
    }

    fun enableAllConfigEnabledPlugins() {
        val configEnabledPlugins = getConfigEnabledPlugins()

        for (plugin in availableScreenElementPlugins) {
            if (configEnabledPlugins.contains(plugin.pluginName)) {
                enable(plugin)
            }
        }

        for (plugin in availableAircraftPlugins) {
            if (configEnabledPlugins.contains(plugin.pluginName)) {
                enable(plugin)
            }
        }
    }

    private fun loadClassNames(
        classNames: ArrayList<String>,
        urlClassLoader: URLClassLoader
    ) {
        availableScreenElementPlugins.clear()
        availableAircraftPlugins.clear()
        for (className in classNames) {
            logger.info("Loading plugin class: $className")

            try {
                val loadedClass = urlClassLoader.loadClass(className)
                val instance = loadedClass.newInstance()

                when (instance) {
                    is ScreenElementPlugin -> {
                        availableScreenElementPlugins.add(instance)
                    }
                    is AircraftPlugin -> {
                        availableAircraftPlugins.add(instance)
                    }
                    else -> {
                        logger.warning("Plugin $className is not a valid Plugin instance")
                    }
                }

            } catch (e: Exception) {
                logger.severe("Failed to load plugin: $className")
                e.printStackTrace()
            }
        }
    }

    private fun getPluginClassNames(urlClassLoader: URLClassLoader): ArrayList<String> {
        val classNames = ArrayList<String>()

        for (url in urlClassLoader.urLs) {
            val jarFile = JarFile(url.file)
            val jarFileEntries: Enumeration<JarEntry> = jarFile.entries()
            val pluginMainClassName = File(jarFile.name).nameWithoutExtension + ".class"

            while (jarFileEntries.hasMoreElements()) {
                val jarFileEntry = jarFileEntries.nextElement()

                if (jarFileEntry.isDirectory || pluginMainClassName != jarFileEntry.name.substringAfterLast("/")) {
                    continue
                }

                val className = jarFileEntry.name
                    .substring(0, jarFileEntry.name.length - ".class".length)
                    .replace('/', '.')

                classNames.add(className)
            }
        }

        return classNames
    }

    private fun getPluginFiles(): Array<URL?>? {
        val pluginFiles = File(pluginDirectory).listFiles { file -> file.name.endsWith(".jar") }
        if (pluginFiles == null) {
            return null
        }

        logger.info("Plugin files found: " + pluginFiles.joinToString { it.name })

        val pluginURLs: Array<URL?> = arrayOfNulls(pluginFiles.size)
        for (i in pluginFiles.indices) {
            pluginURLs[i] = pluginFiles[i].toURI().toURL()
        }

        return pluginURLs
    }

    fun enable(plugin: Any) {
        logger.info("Enabling plugin ${(plugin as BasePlugin).pluginName}")

        when (plugin) {
            is ScreenElementPlugin -> enabledScreenElementPlugins.add(plugin)
            is AircraftPlugin -> {
                logger.info("Disabling all aircraft plugins before enabling ${plugin.pluginName}")
                enabledAircraftPlugins.clear()      // Limit to only ONE enabled
                enabledAircraftPlugins.add(plugin)
            }
            else -> throw InvalidClassException("Unknown plugin type")
        }

        updateConfigWithEnabledPlugins()
    }

    fun disable(plugin: Any) {
        logger.info("Disabling plugin ${(plugin as BasePlugin).pluginName}")

        when (plugin) {
            is ScreenElementPlugin -> enabledScreenElementPlugins.remove(plugin)
            is AircraftPlugin -> enabledAircraftPlugins.remove(plugin)
            else -> throw InvalidClassException("Unknown plugin type")
        }

        updateConfigWithEnabledPlugins()
    }

    fun isEnabled(plugin: BasePlugin): Boolean {
        return getAllEnabled().contains(plugin)
    }

    private fun updateConfigWithEnabledPlugins() {
        Config.ENABLED_PLUGINS = getAllEnabled().joinToString(";") { plugin -> plugin.pluginName }
    }

    private fun getConfigEnabledPlugins() = HashSet<String>(Config.ENABLED_PLUGINS.split(";"))

    fun setupAll() {
        for (plugin: BasePlugin in getAllAvailable()) {
            logger.info("Setting up plugin: ${plugin.pluginName}")
            try {
                plugin.setup()
            } catch (e: Error) {
                logger.severe("Failed to setup plugin: ${plugin.pluginName}. Disabling it for now.")
                availableScreenElementPlugins.remove(plugin)
                availableAircraftPlugins.remove(plugin)
                enabledScreenElementPlugins.remove(plugin)
                enabledAircraftPlugins.remove(plugin)
            }
        }
    }

    fun getAllAvailable(): Set<BasePlugin> {
        return availableScreenElementPlugins + availableAircraftPlugins
    }

    fun getAllAvailableScreenElementPlugins(): Set<ScreenElementPlugin> {
        return availableScreenElementPlugins
    }

    fun getAllAvailableAircraftPlugins(): Set<AircraftPlugin> {
        return availableAircraftPlugins
    }

    fun getAllEnabled(): Set<BasePlugin> {
        return enabledScreenElementPlugins + enabledAircraftPlugins
    }

    fun getAllEnabledScreenElementPlugins(): Set<ScreenElementPlugin> {
        return enabledScreenElementPlugins
    }

    fun getAllEnabledAircraftPlugins(): Set<AircraftPlugin> {
        return enabledAircraftPlugins
    }
}