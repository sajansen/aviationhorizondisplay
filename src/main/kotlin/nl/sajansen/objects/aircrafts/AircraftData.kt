package nl.sajansen.objects.aircrafts

interface AircraftData {
    val aircraftName: String
    val minAirspeed: Double
    val minAirspeedNoFlaps: Double
    val maxAirspeedWarningSpeed: Double
    val maxAirspeed: Double
}