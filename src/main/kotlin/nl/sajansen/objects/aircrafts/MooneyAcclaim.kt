package nl.sajansen.objects.aircrafts

class MooneyAcclaim : AircraftData {
    override val aircraftName: String
        get() = "Mooney Acclaim"
    override val minAirspeed: Double
        get() = 60.0
    override val minAirspeedNoFlaps: Double
        get() = 110.0
    override val maxAirspeedWarningSpeed: Double
        get() = 170.0
    override val maxAirspeed: Double
        get() = 195.0
}