package nl.sajansen.objects

class FlightSimError(message: String?) : Error(message)