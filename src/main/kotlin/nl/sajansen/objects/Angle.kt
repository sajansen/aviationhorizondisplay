package nl.sajansen.objects

class Angle {
    private var degrees: Double = 0.0

    constructor() {
        degrees = 0.0
    }

    constructor(degrees: Double) {
        this.degrees = degrees
    }

    fun getRadians(): Double {
        return Math.toRadians(degrees)
    }

    fun getDegrees(): Double {
        return degrees
    }

    fun setRadians(radians: Double) {
        degrees = Math.toDegrees(radians)
    }

    fun setDegrees(degrees: Double) {
        this.degrees = degrees
    }

    override fun toString(): String {
        return "Angle[${degrees} degrees]"
    }
}