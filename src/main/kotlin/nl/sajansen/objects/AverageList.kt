package nl.sajansen.objects

class AverageList(
    private val maxSize: Int
) {

    private val list = ArrayList<Double>();

    fun add(value: Double) {
        list.add(value)

        if (list.size > maxSize) {
            list.removeAt(0)
        }
    }

    fun average(): Double {
        return list.reduce { acc, value -> acc + value } / list.size
    }

    fun addAndAverage(value: Double): Double {
        add(value)
        return average()
    }
}