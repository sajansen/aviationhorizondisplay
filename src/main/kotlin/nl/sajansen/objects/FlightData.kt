package nl.sajansen.objects

import java.util.*

class FlightData {
    var startDate: Date = Date()
    var lastUpdateDate: Date = Date()

    val bank: Angle = Angle()
    val bankIncreasePrediction: Angle = Angle()
    val pitch: Angle = Angle()
    val pitchIncreasePrediction: Angle = Angle()
    var heading: Angle = Angle()
    var headingIncreasePrediction: Angle = Angle()
    var airspeed: Double = 0.0
    var airspeedIncreasePrediction: Double = 0.0
    var altitude: Double = 0.0
    var altitudeIncreasePrediction: Double = 0.0
    var verticalSpeed: Double = 0.0
    var verticalSPeedIncreasePrediction: Double = 0.0
    var turnCoordinator: Double = 0.0
}
