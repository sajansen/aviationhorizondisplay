package nl.sajansen.objects

class Point {
    var x: Double
    var y: Double

    constructor(x: Double, y: Double) {
        this.x = x
        this.y = y
    }

    constructor(x: Int, y: Int) {
        this.x = x.toDouble()
        this.y = y.toDouble()
    }

    constructor(x: Int, y: Double) {
        this.x = x.toDouble()
        this.y = y
    }

    override fun toString(): String {
        return "Point[${x};${y}]"
    }
}