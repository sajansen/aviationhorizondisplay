package nl.sajansen.objects

class ScreenProperties (
    val width: Int,
    val height: Int,
    val name: String = "Unnamed screen",
    val pitchToScreenFactor: Double = 8.0
)
