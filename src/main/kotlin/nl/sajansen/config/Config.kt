package nl.sajansen.config

object Config {
    var FLIGHT_DATA_UPDATE_TIMER_INIT_DELAY: Long = 4000
    var FLIGHT_DATA_UPDATE_TIMER_INTERVAL: Long = 20
    var SCREEN_UPDATE_TIMER_INIT_DELAY: Long = 0
    var SCREEN_UPDATE_TIMER_INTERVAL: Long = 1000 / 20

    var MAIN_SCREEN_WIDTH: Int = 800
    var MAIN_SCREEN_HEIGHT: Int = 600
    var MAIN_SCREEN_PITCH_TO_SCREEN_FACTOR: Double = 8.0

    var PLUGIN_DIRECTORY: String = "./plugins"
    var ENABLED_PLUGINS: String = ""

    fun load() {
        PropertyLoader.load()
        PropertyLoader.loadConfig(this::class.java)
    }

    fun save() {
        PropertyLoader.saveConfig(this::class.java)
        PropertyLoader.save()
    }
}